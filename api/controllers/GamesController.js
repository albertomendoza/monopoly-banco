/**
 * GamesController
 *
 * @description :: Server-side logic for managing Games
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

 // TODO: Separar los métodos generales en un archivo utils.js

var capitalInicial = 1500.00;
var capitalBancario = 19080.00;

module.exports = {
    all: function(req, res){
        Games.find()
        .exec(function(err, juegos){
            if (err) return res.json(err);
            if (req.isSocket){
                console.log(' Socket ID: ' + sails.sockets.getId(req));
                Games.watch(req);
                Games.subscribe(req.socket, juegos);
            }
        });
        
    },
    create: function (req, res) {
        var game = req.allParams();
        var newJuego = {
            clave: game.clave, 
            jugadores: [{
                jugador: "Banco",
                capital: capitalBancario
            },{
                login: true,
                jugador: game.jugador,
                banquero: true,
                capital: capitalInicial
            }]
        };
        Games.count({clave: game.clave})
        .exec(function(err, juegos){
            if (err) return res.serverError(err);
            if (juegos === 0) {
                Games.create(newJuego)
                .exec(function(err, juego){
                    if (err) return res.json({ estado: false, mensaje:"No se pudo crear el juego" });
                    var newJuego = {
                        id: juego.id,
                        clave: juego.clave,
                        estado: true,
                        banco: true,
                        jugador: {
                            jugador: game.jugador,
                            capital: capitalInicial,
                            banco: {
                                capital: capitalBancario
                            },
                        },
                        jugadores: [{
                                jugador: 'Banco',
                            },{
                                jugador: game.jugador,
                                login: true
                        }],
                        mensaje: "¡Listo!"
                    };
                    Games.publishCreate(newJuego);
                    return res.json(newJuego);
                });
            } else {
                return res.json({
                    estado: false,
                    mensaje: "La clave de juego ya existe"
                });
            }
        });
    },
	attach: function(req, res) {
        var game = req.allParams();
        // TODO: Buscar juego activo con params.clave
        Games.findOne({clave:game.clave}).exec(function(err, juego){
            if (err) res.json(err);
            // TODO: Agregar jugador si juego existe vigente y activo
            if ( (juego !== undefined) && (Object.keys(juego).length > 0) ) {
                var jugadores = juego["jugadores"];
                var busqueda = buscarJugador(game.jugador, jugadores);
                if (busqueda == -1) {
                    var jugador = {
                        login: true,
                        jugador: game.jugador,
                        capital: capitalInicial
                    };
                    jugadores.push(jugador); 
                    var json = {};
                    json["jugadores"] = jugadores;
                    Games.update({clave:game.clave},json)
                    .exec(function(err, juego){
                        if (err) res.json(err);
                        var jugadores = listarJugadores(juego[0].jugadores);
                        var newGame = {
                            id: juego[0].id,
                            clave: juego[0].clave,
                            estado: true,
                            jugador: jugador,
                            accion: 'login',
                            jugadores: jugadores,
                        };
                        Games.publishUpdate(newGame.id, newGame);
                        return res.json(newGame);
                    });
                } else {
                    res.json({
                        estado: false,
                        mensaje: "¡Nombre de jugador existente!"
                    });
                }
            } else {
                // TODO: Crear nuevo juego sino existe clave o juego encontrado no esta vigente
                res.json({
                    estado: false,
                    mensaje: "El juego no existe"
                });
            }
        });
    },
    myAccount: function(req, res){
        var game = req.allParams();
        Games.findOne({id: game.id})
        .exec(function(err, juego){
            if (err) res.json({estado: false, mensaje: "¡Ocurrió un error!"});
            var jugadores = juego["jugadores"];
            var idx = buscarJugador(game.jugador, jugadores);
            var saldo = jugadores[idx].capital;
            var capital = {
                estado: true,
                jugador: game.jugador,
                jugador: saldo,
            };
            if (game.banco) {
                let idx = buscarJugador('Banco', jugadores);
                capital.banco = jugadores[idx].capital;
            }
            return res.json(capital);
        });
    },
    reAttach: function(req, res){
        var game = req.allParams();
        Games.findOne({clave: game.clave})
        .where({estado: true})
        .exec(function(err, juego){
            if (err) return res.json({estado: false, mensaje: "¡Ocurrió un error!"});
            var jugadores = juego["jugadores"];
            var idx = buscarJugador(game.jugador, jugadores);
            jugadores[idx].login = true;
            Games.update({clave: game.clave}, {jugadores: jugadores})
            .exec(function(err, juego){
                if (err) return res.json({estado: false, mensaje: "¡No es posible re ingresar!"});
                var jugador = {
                    login: juego[0].jugadores[idx].login,
                    capital: juego[0].jugadores[idx].capital,
                    jugador: game.jugador
                };
                var jugadores = listarJugadores(juego[0].jugadores);
                var dataPlayer = {
                    id: juego[0].id,
                    clave: juego[0].clave,
                    estado: true,
                    jugador: jugador,
                    accion: 'login',
                    jugadores: jugadores,
                };
                Games.publishUpdate(dataPlayer.id, dataPlayer);
                return res.json(dataPlayer);
            });
        });
    },
    logout: function(req, res){
        var game = req.allParams();
        if (!game.jugador || !game.clave) {
            return res.json({
                estado: false,
                mensaje: "Error al procesar la solicitud"
            });
        }
        Games.findOne({clave: game.clave})
        .exec(function(err, juego){
            if (err) return res.json(err);
            var jugadores = juego.jugadores;
            var jugador = buscarJugador(game.jugador, jugadores);
            if (jugador == -1) return res.json({ estado: false, mensaje: "Jugador no encontrado" });
            if (jugadores[jugador].login === false) return res.json({ estado: false, mensaje: "Jugador no activo" });
            jugadores[jugador].login = false;
            Games.update({clave: game.clave}, {jugadores: jugadores})
            .exec(function(err, juego){
                if (err) return res.json(err);
                var jugador = {
                    id: juego[0].id,
                    accion: 'logout',
                    jugador: game.jugador,
                    clave: game.clave
                };
                Games.publishUpdate(jugador.id, jugador);
                return res.json(jugador);
            });
        });
    },
    transfer: function(req, res){
        var game = req.allParams();
        if ( !game.origen || !game.destino || !game.monto || game.monto < 0) {
            return res.json({
                estado: false,
                mensaje: "No es posible realizar la transferencia"
            });
        }
        Games.findOne({id:game.id})
        .exec(function(err, juego){
            // TODO: Validar que el juego se encuentre vigente
            var jugadores = juego.jugadores;
            var cntOrigen = buscarJugador(game.origen, jugadores);
            var cntDestino = buscarJugador(game.destino, jugadores);
            if (cntOrigen === -1 || cntDestino === -1 ) {
                return res.status(200).json({
                    estado: false,
                    mensaje: "La cuenta destino no ha sido encontrada"
                });
            }
            jugadores[cntOrigen].capital -= Number(game.monto);
            jugadores[cntDestino].capital += Number(game.monto);
            
            Games.update({id:game.id}, {jugadores: jugadores})
            .exec(function(err, saldos){
                var transferencia = {
                    id: game.id,
                    clave: saldos[0].clave,
                    origen: game.origen,
                    destino: game.destino,
                    monto: game.monto,
                    accion: 'transfer',
                    estado: true, // TODO: definir el estado de la transferencia
                    saldo: saldos[0].jugadores[cntOrigen]
                };
                Games.publishUpdate(transferencia.id,transferencia);
                return res.json(transferencia);
            });
        });
        // TODO: Retornar nueva información vía socket
    }
};

function buscarJugador(jugador,jugadores){
    var busqueda = jugadores.filter(function(obj){
        return (obj.jugador === jugador);
    });
       
    var searchValue = busqueda[0], index = -1; 
    _.each(jugadores, function(data, idx) {
        if (_.isEqual(data, searchValue)) {
            index = idx; 
            return; 
        } 
    }); 
    return index;
}

function listarJugadores(jugadores){
    jugadores.forEach(jugador => {
        delete jugador.capital;
    });
    return jugadores;
}