/**
 * Games.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
    attributes: {
        estado: { type: 'boolean', defaultsTo: true, unique: true },
        clave: { type: "String" },
        jugadores: { type: "Array", defaultsTo: [] },
        // log: { type: "Array", defaultsTo: [] }
    }
};

